#!/usr/bin/env node

const fs = require('fs')

const latest = JSON.parse(fs.readFileSync('/dev/stdin'))

const keys = ['lastUpdated', 'unit']

const features = latest.results.map((result) => {
  const properties = {
    name: result.location,
    city: result.city,
  }

  result.measurements.forEach((measurement) => {
    properties[measurement.parameter] = measurement.value

    keys.forEach((key) => {
      if (key in measurement) {
        properties[`${measurement.parameter}.${key}`] = measurement[key]
      }
    })

    if ('averagingPeriod' in measurement) {
      if ('value' in measurement.averagingPeriod && 'unit' in measurement.averagingPeriod) {
        properties[`${measurement.parameter}.averagingPeriod`] = measurement.averagingPeriod.value
        properties[`${measurement.parameter}.averagingPeriodUnit`] = measurement.averagingPeriod.unit
      }
    }
  })

  const feature = {
    type: 'Feature',
    properties: properties,
  }

  if (result.coordinates && result.coordinates.longitude && result.coordinates.latitude) {
    feature.geometry = {
      type: 'Point',
      coordinates: [result.coordinates.longitude, result.coordinates.latitude]
    }
  } else {
    feature.geometry = null
  }

  return feature
})

const featureCollection = {
  type: 'FeatureCollection',
  features: features
}

process.stdout.write(JSON.stringify(featureCollection, null, 2))
