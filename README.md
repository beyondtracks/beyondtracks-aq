# beyondtracks-aq

Processing of air quality data for use in BeyondTracks.com

## Usage

- `make dist/latest.json`
- `make dist/sources.json`
- `make dist/parameters.json`
- `make dist/latest.geojson`

## Air Quality Categories

`aqCategories.json` contains a machine readable classification for Good, Moderate, Poor, Very poor and Hazardous values.

Based on EPA Victoria ranges at https://www.epa.vic.gov.au/for-community/monitoring-your-environment/about-epa-airwatch/calculate-air-quality-categories
