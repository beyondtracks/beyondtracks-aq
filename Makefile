all: dist/sources.json dist/parameters.json dist/latest.geojson

dist/latest.json:
	mkdir -p dist
	wget -O $@ 'https://api.openaq.org/v1/latest?country=AU&limit=10000'

dist/sources.json:
	mkdir -p dist
	wget -O - 'https://api.openaq.org/v1/sources?limit=10000' | jq '.results[] | select(.country | contains("AU")) | [.]' > $@

dist/parameters.json:
	mkdir -p dist
	wget -O - 'https://api.openaq.org/v1/parameters' | jq '.results' > $@

dist/latest.geojson: dist/latest.json
	./latestAsGeoJSON.js < $< > $@
